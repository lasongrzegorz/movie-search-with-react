import React from 'react';
import './App.css';
import SearchMovies from "./searchMovies";
require('dotenv').config()

class App extends React.Component {
  render() {
    return (
        <div className="container">
        <h1 className="title"> Movie search with React </h1>
          <SearchMovies/>
        </div>
    );
  }
}

export default App;
