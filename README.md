# The Movies Search with React
A very simple React app that uses API data for display movies info.

## Table of contents
* [General info](#general-info)

## General info

The Movies Search App is fed with [TMDB API](https://www.themoviedb.org/) 
where the front was designed using [Create React App](https://github.com/facebook/create-react-app).
The App allows only to search for movie: poster, released date and rating.
